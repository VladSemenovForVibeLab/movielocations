package com.semenov.MovieLocation.Model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "cities")
@NoArgsConstructor
public class City {
    @Id
    private int city_id;
    private String city_name;
}
