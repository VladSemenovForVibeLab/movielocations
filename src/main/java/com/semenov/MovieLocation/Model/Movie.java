package com.semenov.MovieLocation.Model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "movies")
public class Movie {
    @Id
    private int movie_id;
    private String movie_name;
    private String description;
    private String poster_url;
    private String release_year;
    private String director;
    private String genre;
    private int duration_minutes;
}
