package com.semenov.MovieLocation.Controller;

import com.semenov.MovieLocation.Model.Movie;
import com.semenov.MovieLocation.Service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("movies")
public class MovieController {
    @Autowired
    private MovieService movieService;

    @GetMapping
    public ResponseEntity<List<Movie>> getAllTheMovies(){
        return ResponseEntity.ok(movieService.getAllMovies());
    }
}
