package com.semenov.MovieLocation.Service;

import com.semenov.MovieLocation.Model.City;
import com.semenov.MovieLocation.Repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {
    @Autowired
    private CityRepository cityRepository;
    public List<City> getAllCities(){
        return cityRepository.findAll();
    }
}
