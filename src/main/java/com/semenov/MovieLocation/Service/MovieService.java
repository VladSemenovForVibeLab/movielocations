package com.semenov.MovieLocation.Service;

import com.semenov.MovieLocation.Model.Movie;
import com.semenov.MovieLocation.Repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;
    public List<Movie> getAllMovies(){
        return movieRepository.findAll();
    }
}
