INSERT INTO movies (movie_id, movie_name, description, poster_url, release_year, director, genre, duration_minutes)
VALUES (1, 'Inception', 'A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.', 'https://example.com/inception_poster.jpg', '2010', 'Christopher Nolan', 'Action, Sci-Fi', 148);

INSERT INTO movies (movie_id, movie_name, description, poster_url, release_year, director, genre, duration_minutes)
VALUES (2, 'The Shawshank Redemption', 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.', 'https://example.com/shawshank_redemption_poster.jpg', '1994', 'Frank Darabont', 'Crime, Drama', 142);

INSERT INTO movies (movie_id, movie_name, description, poster_url, release_year, director, genre, duration_minutes)
VALUES (3, 'Pulp Fiction', 'The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.', 'https://example.com/pulp_fiction_poster.jpg', '1994', 'Quentin Tarantino', 'Crime, Drama', 154);

INSERT INTO movies (movie_id, movie_name, description, poster_url, release_year, director, genre, duration_minutes)
VALUES (4, 'The Dark Knight', 'When the menace known as The Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham. Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.', 'https://example.com/dark_knight_poster.jpg', '2008', 'Christopher Nolan', 'Action, Crime, Drama', 152);

INSERT INTO movies (movie_id, movie_name, description, poster_url, release_year, director, genre, duration_minutes)
VALUES (5, 'Fight Club', 'An insomniac office worker and a devil-may-care soapmaker form an underground fight club that evolves into something much, much more.', 'https://example.com/fight_club_poster.jpg', '1999', 'David Fincher', 'Drama', 139);

INSERT INTO movies (movie_id, movie_name, description, poster_url, release_year, director, genre, duration_minutes)
VALUES (6, 'Forrest Gump', 'The presidencies of Kennedy and Johnson, the events of Vietnam, Watergate and other historical events unfold through the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.', 'https://example.com/forrest_gump_poster.jpg', '1994', 'Robert Zemeckis', 'Drama, Romance', 142);

INSERT INTO movies (movie_id, movie_name, description, poster_url, release_year, director, genre, duration_minutes)
VALUES (7, 'The Matrix', 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.', 'https://example.com/matrix_poster.jpg', '1999', 'Lana Wachowski, Lilly Wachowski', 'Action, Sci-Fi', 136);

INSERT INTO cities (city_id, city_name) VALUES (1, 'New York');
INSERT INTO cities (city_id, city_name) VALUES (2, 'Los Angeles');
INSERT INTO cities (city_id, city_name) VALUES (3, 'Chicago');
INSERT INTO cities (city_id, city_name) VALUES (4, 'San Francisco');
INSERT INTO cities (city_id, city_name) VALUES (5, 'Miami');